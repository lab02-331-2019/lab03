import { TestBed } from '@angular/core/testing';

import { StudentsFileImpl02Service } from './student-file-impl02.service';

describe('StudentDataImpl02Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsFileImpl02Service = TestBed.get(StudentsFileImpl02Service);
    expect(service).toBeTruthy();
  });
});
