import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsFileImpl02Service extends StudentService {

  constructor(private http: HttpClient) {
    super();
   }
  
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('http://se331.s3-ap-southeast-1.amazonaws.com/people.json');
  }
}
